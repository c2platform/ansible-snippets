# CHANGELOG

## 0.2.6 ()

* New snippets `c2_files_vars_default` for roles that include [c2platform.core.files](https://gitlab.com/c2platform/ansible-collection-core/-/tree/master/roles/files).
* Snippets are also for `ansible` language ( not only for `yaml` ).

## 0.2.5 (2022-09-21)

* New snippets `c2_include_role_cacerts2`, `c2_cacerts2_certificates`, `c2_cacerts2_ca_vars`, `c2_playbook` for [c2platform.core.cacerts2](https://gitlab.com/c2platform/ansible-collection-core/-/tree/master/roles/cacerts2) role.

## 0.2.4 (2022-09-14)

* First release from GitLab
